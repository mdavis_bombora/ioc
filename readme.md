# Implementing IoC Container #

## Behaviours ##


- Register services and their implementations.
  - Concrete classes
  - Interfaces or abstract classes
  - Factory methods
- Resolve a service
- Generic services
- Resolve an array or enumeration
- Property injection
- Service lifetime
- Object disposal

## How ##

`System.Reflection` namespace

```c#
System.Activator.CreateInstance(typeof(Service));

System.Reflection.ConstructorInfo
```

## Implementation ##

```c#
  IContainer container = new Container();
  
  container.Register(Type service) : IContainer;
  container.Register<Service>() : IContainer;
  container.Register<IService, Service>() : IContainer;
  container.Register(Type service, Type impl) : IContainer;
  container.Register<TService>(Func<IContainer, TService> factory) : IContainer;
  
  container.Resolve<Service>() : Service;
  container.Resolve<IService>() : IService;
  container.ResolveAll<IService>() : IEnumerable<IService>;
```