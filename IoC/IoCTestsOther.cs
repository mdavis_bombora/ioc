using System;
using FluentAssertions;
using NUnit.Framework;

namespace IoC
{
    class IoCTestsOther
    {
        private IContainer _container;

        [SetUp]
        public void SetUp() => _container = new Container();

        [Test]
        public void ThrowsErrorIfTypeCannotBeResolved()
        {
            Action act = () => _container.Resolve<int>();

            act.Should().Throw<Exception>();
        }
        
    }
}