using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace IoC
{
    interface IContainer
    {
        // IContainer Register(Type service);
        // IContainer Register<T>();
        IContainer Register<T>(T instance);
        IContainer Register<TService, TImpl>() where TImpl : TService;
        IContainer Register(Type service, Type impl);
        IContainer Register<TService>(Func<IContainer, TService> factory);

        TService Resolve<TService>();
        IEnumerable<TService> ResolveAll<TService>();
    }
    
    class Container : IContainer
    {
        private IDictionary<Type, object> instances = new Dictionary<Type, object>(); 
        private IDictionary<Type, Func<IContainer, object>> factoryMethods = new Dictionary<Type, Func<IContainer, object>>();
        private IDictionary<Type, Type> typeRegistrations = new Dictionary<Type, Type>();

        public IContainer Register<T>(T instance)
        {
            instances.Add(typeof(T), instance);

            return this;
        }

        public IContainer Register<TService, TImpl>() where TImpl : TService =>
            Register(typeof(TService), typeof(TImpl));

        public IContainer Register(Type service, Type impl)
        {
            typeRegistrations.Add(service, impl);

            return this;
        }

        public IContainer Register<TService>(Func<IContainer, TService> factory)
        {
            factoryMethods.Add(typeof(TService), c => factory(c));

            return this;
        }

        public TService Resolve<TService>() => (TService) Resolve(typeof(TService));

        private object Resolve(Type type)
        {
            var instance = AttemptToResolveInstance(type)
                           ?? AttemptToResolveFactoryMethod(type)
                           ?? AttemptToResolveType(type);

            instances[type] = instance ?? throw new Exception($"Could not resolve {type}");

            return instance;
        }

        private object AttemptToResolveInstance(Type t) =>
            instances.ContainsKey(t) ? instances[t] : null;

        private object AttemptToResolveFactoryMethod(Type t) =>
            factoryMethods.ContainsKey(t) ? factoryMethods[t](this) : null;

        private object AttemptToResolveType(Type t)
        {
            if (typeRegistrations.ContainsKey(t))
            {
                var instanceType = typeRegistrations[t];

                var constructor = instanceType.GetConstructors(BindingFlags.Instance | BindingFlags.Public).First();

                var parameters = constructor.GetParameters()
                    .Select(p => Resolve(p.ParameterType))
                    .ToArray();

                return constructor.Invoke(parameters);
            }

            return null;
        }

        public IEnumerable<TService> ResolveAll<TService>()
        {
            throw new NotImplementedException();
        }
    }
}