using FluentAssertions;
using NUnit.Framework;

namespace IoC
{
    class IoCTests
    {
        private IContainer _container;

        [SetUp]
        public void SetUp() => _container = new Container();

        [Test]
        public void RegisterAndResolveFactoryMethodRegistration()
        {
            var instance = new ServiceImpl();

            _container.Register<IService>(_ => instance).Resolve<IService>().Should().BeSameAs(instance);
        }
        
        [Test]
        public void RegisterAndResolveInstanceRegistration()
        {
            var instance = new ServiceImpl();

            _container.Register<IService>(instance).Resolve<IService>().Should().BeSameAs(instance);
        }

        [Test]
        public void RegisterAndResolveServiceRegisteredByType() =>
            _container
                .Register(typeof(IService), typeof(ServiceImpl))
                .Resolve<IService>().Should().BeOfType<ServiceImpl>();
        
        [Test]
        public void RegisterAndResolveServiceRegisteredByGeneric() =>
            _container
                .Register<IService, ServiceImpl>()
                .Resolve<IService>().Should().BeOfType<ServiceImpl>();

        [Test]
        public void ResolvesTheSameTypeInTheSameContext()
        {
            var firstResolved = _container
                .Register<IService, ServiceImpl>()
                .Resolve<IService>();

            _container.Resolve<IService>().Should().BeSameAs(firstResolved);
        }
        
        [Test]
        public void RegisterAndResolveComplexType()
        {
            var impl = _container
                    .Register<ISubSubDependency, SubSubDependency>()
                    .Register<ISubDependency, SubDependency>()
                    .Register<IService, ComplexServiceImpl>()
                    .Resolve<IService>().Should().BeOfType<ComplexServiceImpl>().Subject;

            impl.SubDependency.SubSubDependency.Should().BeSameAs(impl.SubSubDependency);
        }
        
        [Test]
        public void RegisterAndResolveFactoryMethodRegistrationWhileResolvingDependencies()
        {
            var impl = _container
                .Register<ISubSubDependency, SubSubDependency>()
                .Register<ISubDependency, SubDependency>()
                .Register<IService>(c =>
                    new ComplexServiceImpl(
                        c.Resolve<ISubDependency>(), c.Resolve<ISubSubDependency>()))
                .Resolve<IService>().Should().BeOfType<ComplexServiceImpl>().Subject;

            impl.SubDependency.SubSubDependency.Should().BeSameAs(impl.SubSubDependency);
        }
    }

    interface IService { }
    
    class ServiceImpl : IService
    {
        
    }
    
    class ComplexServiceImpl : IService
    {
        public ISubDependency SubDependency { get; }
        public ISubSubDependency SubSubDependency { get; }

        public ComplexServiceImpl(ISubDependency subDependency, ISubSubDependency subSubDependency)
        {
            SubDependency = subDependency;
            SubSubDependency = subSubDependency;
        }
    }

    interface ISubDependency
    {
        ISubSubDependency SubSubDependency { get; }
    }

    class SubDependency : ISubDependency
    {
        public ISubSubDependency SubSubDependency { get; }

        public SubDependency(ISubSubDependency subSubDependency) => SubSubDependency = subSubDependency;
    }
    
    interface ISubSubDependency {}

    class SubSubDependency : ISubSubDependency { }
}